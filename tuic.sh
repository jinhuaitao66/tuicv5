#!/bin/bash

install_tuic_arm() {
    # 创建目录并进入
    mkdir -p /opt/tuic && cd /opt/tuic || exit

    # 下载 tuic-server 二进制文件
    wget https://gitlab.com/jinhuaitao66/tuicv5/-/raw/main/ARM/tuic-server-1.0.0-aarch64-unknown-linux-gnu -O tuic-server

    # 设置 tuic-server 可执行权限
    chmod +x tuic-server

    # 下载 SSL 证书和密钥
    curl -o bing.crt https://gitlab.com/jinhuaitao66/tuicv5/-/raw/main/ARM/bing.crt
    curl -o bing.key https://gitlab.com/jinhuaitao66/tuicv5/-/raw/main/ARM/bing.key

    # 下载配置文件
    curl -o config.json https://gitlab.com/jinhuaitao66/tuicv5/-/raw/main/ARM/config.json

    # 下载 systemd 服务文件
    curl -o /lib/systemd/system/tuic.service https://gitlab.com/jinhuaitao66/tuicv5/-/raw/main/ARM/tuic.service

    # 启用并启动服务
    systemctl enable --now tuic.service

    # 重启服务
    systemctl restart tuic

    # 检查服务状态
    systemctl status tuic
}

install_tuic_x86() {
    # 创建目录并进入
    mkdir -p /opt/tuic && cd /opt/tuic || exit

    # 下载 tuic-server 二进制文件
    wget https://gitlab.com/jinhuaitao66/tuicv5/-/raw/main/X86/tuic-server-1.0.0-x86_64-unknown-linux-gnu -O tuic-server

    # 设置 tuic-server 可执行权限
    chmod +x tuic-server

    # 下载 SSL 证书和密钥
    curl -o bing.crt https://gitlab.com/jinhuaitao66/tuicv5/-/raw/main/X86/bing.crt
    curl -o bing.key https://gitlab.com/jinhuaitao66/tuicv5/-/raw/main/X86/bing.key

    # 下载配置文件
    curl -o config.json https://gitlab.com/jinhuaitao66/tuicv5/-/raw/main/X86/config.json

    # 下载 systemd 服务文件
    curl -o /lib/systemd/system/tuic.service https://gitlab.com/jinhuaitao66/tuicv5/-/raw/main/X86/tuic.service

    # 启用并启动服务
    systemctl enable --now tuic.service

    # 重启服务
    systemctl restart tuic

    # 检查服务状态
    systemctl status tuic
}

uninstall_tuic() {
    # 停止服务
    systemctl stop tuic

    # 禁用服务
    systemctl disable tuic

    # 移除服务文件
    rm /lib/systemd/system/tuic.service

    # 移除安装的文件和目录
    rm -rf /opt/tuic

    # 重新加载 systemd 以应用更改
    systemctl daemon-reload

    echo "TuiC 已卸载。"
}

# 函数：提示用户选择
prompt_choice() {
    read -p "请输入数字选择操作：1. 安装 ARM 版本  2. 安装 X86 版本  3. 卸载 : " choice
    case "$choice" in
        1) install_tuic_arm ;;
        2) install_tuic_x86 ;;
        3) uninstall_tuic ;;
        *) echo "无效选择。请键入 '1'、'2' 或 '3'。" ;;
    esac
}

# 提示用户进行选择
prompt_choice
